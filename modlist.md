# modlist


|name|description|
|-|-|
|[Ad Astra](https://modrinth.com/mod/3ufwT9JF)|Live long and prosper, Ad Astra!|
|[AdvancementInfo](https://modrinth.com/mod/G1epq3jN)|Show more information about advancement requirements|
|[Advancement Plaques](https://modrinth.com/mod/9NM0dXub)|Replace those boring advancement popups with something flashier.|
|[AE2 EMI Crafting Integration](https://modrinth.com/mod/eVAp8Nkw)|Adds support for crafting with EMI on Applied Energistics 2 terminals|
|[Applied Energistics 2 Wireless Terminals](https://modrinth.com/mod/pNabrMMw)|An addon for Applied Energistics 2 that adds wireless versions of several Terminals|
|[Affectionate](https://modrinth.com/mod/VYOybYzW)|A mod about player interactions, made for Modfest: Singularity.|
|[Alternate Current](https://modrinth.com/mod/r0v8vy1s)|An efficient and non-locational redstone dust implementation|
|[Amecs](https://modrinth.com/mod/rcLriA4v)|Improves your keys & controls setup by providing modifier keys and making multi-functional keys a thing.|
|[Animatica](https://modrinth.com/mod/PRN43VSY)|A mod implementing the OptiFine/MCPatcher animated texture format|
|[AntiGhost](https://modrinth.com/mod/Jw3Wx1KR)|Get rid of ghost blocks by requesting resends from the server|
|[AppleSkin](https://modrinth.com/mod/EsAfCjCV)|Food/hunger-related HUD improvements|
|[Applied Botanics](https://modrinth.com/mod/545hUrw9)|mana through ae2, what could go wrong|
|[Applied Energistics 2](https://modrinth.com/mod/XxWD5pD3)|AE2: A popular automation and storage mod|
|[Architectury API](https://modrinth.com/mod/lhGA9TYQ)|An intermediary api aimed to ease developing multiplatform mods.|
|[Argonauts](https://modrinth.com/mod/bb2EpKpx)|A guild and party mod to work and play together with your teammates on a server!|
|[Athena](https://modrinth.com/mod/b1ZV3DIJ)|A crossplatform (Forge/Fabric) solution to connected block textures for 1.19.4+|
|[bad packets](https://modrinth.com/mod/ftdbN0KK)|Bad Packets allows packet messaging between different modding platforms.|
|[BetterF3](https://modrinth.com/mod/8shC1gFX)|BetterF3 is a mod that replaces Minecraft's original debug HUD with a highly customizable, more human-readable HUD.|
|[Better Mount HUD](https://modrinth.com/mod/kqJFAPU9)|Improves the ingame HUD while riding a mount|
|[Better Ping Display [Fabric]](https://modrinth.com/mod/MS1ZMyR7)|Adds a configurable numerical ping display to the player list|
|[Better Statistics Screen](https://modrinth.com/mod/n6PXGAoM)|A Minecraft mod that improves the statistics screen and makes it more useful.|
|[Blåhaj](https://modrinth.com/mod/Yl6uPZkr)|Soft toy, shark mod, 16px.|
|[Borderless Mining](https://modrinth.com/mod/kYq5qkSL)|Changes Fullscreen to use a borderless window.|
|[Botania](https://modrinth.com/mod/pfjLUfGv)|An innovative natural magic themed tech mod|
|[Botarium](https://modrinth.com/mod/2u6LRnMa)|A crossplatform API for devs that makes transfer and storage of items, fluids and energy easier, as well as some other helpful things|
|[Building Wands](https://modrinth.com/mod/XkisZUfp)|Building wands with different modes|
|[Cadmus](https://modrinth.com/mod/fEWKxVzh)|A land claiming mod that allows users to claim land to protect your home from thieves,  bandits and monsters, and admins to claim land with region flags and advanced protection|
|[Cardinal Components API](https://modrinth.com/mod/K01OU20C)|A data attachment API that is easy, modular, and extremely fast.|
|[Carry On](https://modrinth.com/mod/joEfVgkn)|Carry On allows you to pick up Tile Entities and Mobs and carry them around!|
|[CC: Tweaked](https://modrinth.com/mod/gu7yAYhd)|ComputerCraft fork adding programmable computers, turtles and more to Minecraft.|
|[CC:C Bridge](https://modrinth.com/mod/fXt291FO)|Adds compatibility between the ComputerCraft and Create mod with peripherals!|
|[Charmonium](https://modrinth.com/mod/bpii4Xsa)|Ambient and environmental sounds in keeping with vanilla Minecraft|
|[Chipped](https://modrinth.com/mod/BAscRYKm)|Every block deserves a friend.|
|[Chunky](https://modrinth.com/mod/fALzjamp)|Pre-generates chunks, quickly and efficiently|
|[CIT Resewn](https://modrinth.com/mod/otVJckYQ)|Re-implements MCPatcher's CIT (custom item textures from optifine resource packs)|
|[Cloth Config API](https://modrinth.com/mod/9s6osm5g)|Configuration Library for Minecraft Mods|
|[Continuity](https://modrinth.com/mod/1IjD5062)|A Fabric mod that allows for efficient connected textures|
|[Create Fabric](https://modrinth.com/mod/Xbc0uyRg)|Building Tools and Aesthetic Technology|
|[Create Enchantment Industry Fabric](https://modrinth.com/mod/AEZO385x)|Automatic Enchanting, with Create|
|[Create Crafts & Additions](https://modrinth.com/mod/kU1G12Nn)|Create Crafts & Additions extends Create and acts as a bridge between electricity and kinetic energy|
|[Creeper Spores](https://modrinth.com/mod/WSdgoMoj)|Griefless creepers with a few tricks|
|[Cull Less Leaves](https://modrinth.com/mod/iG6ZHsUV)|Cull leaves while looking hot!|
|[Debugify](https://modrinth.com/mod/QwxR6Gcd)|Fixes Minecraft bugs found on the bug tracker|
|[When Dungeons Arise](https://modrinth.com/mod/8DfbfASn)|Adds various elegant -and likely hostile- roguelike dungeons and structures that generate on your worlds!|
|[Dynamic FPS](https://modrinth.com/mod/LQ3K71Q1)|Reduce resource usage while Minecraft is in the background or idle.|
|[e4mc](https://modrinth.com/mod/qANg5Jrr)|Open a LAN server to anyone, anywhere, anytime.|
|[Ears (+ Snouts/Muzzles, Tails, Horns, Wings, and More)](https://modrinth.com/mod/mfzaZK3Z)|More skin customization options for just about every version + skin back​ports/fixes for pre-1.9 versions.|
|[EMI](https://modrinth.com/mod/fRiHVvU7)|A featureful and accessible item and recipe viewer|
|[EMI Loot](https://modrinth.com/mod/qbbO7Jns)|A loot drop (chest, block, entity) plugin for the EMI Recipe and Item viewer.|
|[Emoji Type](https://modrinth.com/mod/q7vRRpxU)|Allows you to easily type emoji and emoticons in game.|
|[Enhanced Block Entities](https://modrinth.com/mod/OVuFYfre)|Reduce FPS lag with block entities, as well as customize them with resource packs|
|[[EMF] Entity Model Features](https://modrinth.com/mod/4I1XuqiY)|EMF is an, OptiFine format, Custom Entity Model replacement mod available for Fabric and Forge.|
|[[ETF] Entity Texture Features](https://modrinth.com/mod/BVzZfTc1)|Emissive, Random & Custom texture support for entities in resourcepacks just like Optifine but for Fabric|
|[Entity Culling](https://modrinth.com/mod/NNAgCjsB)|Using async path-tracing to hide Block-/Entities that are not visible|
|[Equipment Compare](https://modrinth.com/mod/CYSUVOdj)|Makes it easier to compare equipment by showing a tooltip for what you're already wearing.|
|[Expanded Delight](https://modrinth.com/mod/e9V6wFcR)|An addon mod for Farmer's Delight Fabric that adds many more crops and functionality to the base mod|
|[Extended Drawers](https://modrinth.com/mod/AhtxbnpG)|Adds drawers and more|
|[EMI Addon: Extra Mod Integrations](https://modrinth.com/mod/bpRHnWUb)|EMI addon adding support for as many mods as possible|
|[FabricSkyboxes](https://modrinth.com/mod/YBz7DOs8)|Allows resource packs to define custom skyboxes.|
|[Fabric Tailor](https://modrinth.com/mod/g8w1NapE)|A server-side / singleplayer skin & cape changing mod for fabric.|
|[Fabrishot](https://modrinth.com/mod/3qsfQtE9)|Take insanely large screenshots because why not|
|[Falling Leaves](https://modrinth.com/mod/WhbRG4iK)|Adds a neat little particle effect to leaf blocks|
|[FallingTree](https://modrinth.com/mod/Fb4jn8m6)|Break down your trees by only cutting one piece of it|
|[Farmer's Delight [Fabric]](https://modrinth.com/mod/4EakbH8e)|Farmer's Delight is a mod that gently expands upon farming and cooking in Minecraft.|
|[FastQuit](https://modrinth.com/mod/x1hIzbuY)|Lets you return to the Title Screen early while your world is still saving in the background!|
|[FerriteCore](https://modrinth.com/mod/uXXizFIs)|Memory usage optimizations|
|[Forge Config API Port](https://modrinth.com/mod/ohNO6lps)|NeoForge's & Forge's config systems provided to other modding ecosystems. Designed for a multiloader architecture.|
|[FabricSkyBoxes Interop](https://modrinth.com/mod/HpdHOPOp)|FabricSkyBoxes Interoperability for MCPatcher/OptiFine Skies|
|[Gravestones](https://modrinth.com/mod/ssUbhMkL)|A gravestones mod for fabric with tons of config options, an API, and more! |
|[Heracles](https://modrinth.com/mod/lo90fZoB)|A tree-style questing mod, allowing pack makers to make and include completable quests for their players|
|[Item Highlighter](https://modrinth.com/mod/cVNW5lr6)|Highlights newly picked-up items. Simple and convenient.|
|[Hold That Chunk](https://modrinth.com/mod/LXJlc5WJ)|Delays client chunk unloading|
|[Icarus](https://modrinth.com/mod/Dw7M6XKW)|Adds a bunch of colourful and unique wings that can be worn in the Trinkets Cape slot.|
|[Iceberg](https://modrinth.com/mod/5faXoLqX)|A modding library that contains new events, helpers, and utilities to make modder's lives easier.|
|[ImmediatelyFast](https://modrinth.com/mod/5ZwdcRci)|Speed up immediate mode rendering in Minecraft|
|[Incendium](https://modrinth.com/mod/ZVzW5oNS)|Incendium is a datapack that completely revamps the nether, way more than what Minecraft did in the 1.16 Nether Update.|
|[Indium](https://modrinth.com/mod/Orvt0mRa)|Sodium addon providing support for the Fabric Rendering API, based on Indigo|
|[Inspecio](https://modrinth.com/mod/a93H3mKU)|A Minecraft mod which adds more tooltips.|
|[Iris Shaders](https://modrinth.com/mod/YL57xq9U)|A modern shaders mod for Minecraft intended to be compatible with existing OptiFine shader packs|
|[Item Borders](https://modrinth.com/mod/b1fMg6sH)|Add colored borders to inventory slots to make your rare items stand out!|
|[Jade 🔍](https://modrinth.com/mod/nvQzSEkH)|Shows information about what you are looking at. (Hwyla/Waila fork for Minecraft 1.16+)|
|[Jade Addons (Fabric)](https://modrinth.com/mod/fThnVRli)|Jade 🔍's additional mod supports for Fabric|
|[JamLib](https://modrinth.com/mod/IYY9Siz8)|The platform-agnostic, Architectury based library used in all of JamCoreModding's mods|
|[Just Enough Items](https://modrinth.com/mod/u6dRKJwZ)|JEI - View Items and Recipes|
|[JourneyMap](https://modrinth.com/mod/lfHFW1mp)|Real-time mapping in game or in a web browser as you explore.|
|[Krypton](https://modrinth.com/mod/fQEb0iXm)|A mod to optimize the Minecraft networking stack|
|[LambdaBetterGrass](https://modrinth.com/mod/2Uev7LdA)|A Minecraft mod which adds better grass and snow to the game.|
|[LambDynamicLights](https://modrinth.com/mod/yBW8D80W)|A dynamic lights mod for Fabric.|
|[Language Reload](https://modrinth.com/mod/uLbm7CG6)|Reduces load times and adds fallbacks for languages|
|[LazyDFU](https://modrinth.com/mod/hvFnDODi)|Makes the game boot faster by deferring non-essential initialization|
|[Legendary Tooltips](https://modrinth.com/mod/atHH8NyV)|Give your rare items a fancier tooltip! Also adds additional tooltip configuration options.|
|[Lighty](https://modrinth.com/mod/yjvKidNM)|The Light Overlay Mod with a twist!|
|[Lithium](https://modrinth.com/mod/gvQqBUqZ)|No-compromises game logic/server optimization mod|
|[Memory Leak Fix](https://modrinth.com/mod/NRjRiSSD)|A mod that fixes random memory leaks for both the client and server|
|[Mixin Conflict Helper](https://modrinth.com/mod/MR1VIQJJ)|User-friendly errors for Mixin conflicts.|
|[MixinTrace](https://modrinth.com/mod/sGmHWmeL)|Adds a list of mixins in the stack trace to crash reports |
|[Model Gap Fix](https://modrinth.com/mod/QdG47OkI)|Fixes gaps in Block Models and Item Models|
|[Mod Menu](https://modrinth.com/mod/mOgUt4GM)|Adds a mod menu to view the list of mods you have installed.|
|[More Chat History](https://modrinth.com/mod/8qkXwOnk)|Increases the maximum length of chat history.|
|[More Culling](https://modrinth.com/mod/51shyZVL)|A mod that changes how multiple types of culling are handled in order to improve performance|
|[More Culling Extra](https://modrinth.com/mod/dFKMFBrn)|A MoreCulling extension mod with smaller changes I don't want to put into MoreCulling. MoreCulling API example mod|
|[Mouse Wheelie](https://modrinth.com/mod/u5Ic2U1u)|A "small" clientside mod featuring item scrolling, inventory sorting, item refilling and more!|
|[No More Useless Keys - NMUK](https://modrinth.com/mod/YCcdA1Lp)|Allows you to add alternative key combinations to every key binding!|
|[No Telemetry](https://modrinth.com/mod/hg77g4Pw)|Disable the telemetry introduced in 21w38a|
|[No Chat Reports](https://modrinth.com/mod/qQyHxfxd)|Makes chat unreportable (where possible)|
|[Nullscape](https://modrinth.com/mod/LPjGiSO4)|Nullscape keeps the End as a barren, depressing, and bleak place: as it should be, and always will be.|
|[Numismatic Overhaul](https://modrinth.com/mod/ZXm8hVxN)|Terraria-style currency in Minecraft|
|[Ok Zoomer](https://modrinth.com/mod/aXf2OSFU)|Adds a highly-configurable zoom key for Quilt. The zoom is yours!|
|[oωo (owo-lib)](https://modrinth.com/mod/ccKDOlHs)|A general utility, GUI and config library for modding on Fabric and Quilt|
|[Patchouli](https://modrinth.com/mod/nU0bVIaL)|Accessible, Data-Driven, Dependency-Free Documentation for Minecraft Modders and Pack Makers|
|[Ping Wheel](https://modrinth.com/mod/QQXAdCzh)|Allows players to temporarily mark locations and entities|
|[Polymorphic Energistics](https://modrinth.com/mod/VS1a14jA)|Polymorph support for Applied Energistics 2.|
|[Polymorph](https://modrinth.com/mod/tagwiZkJ)|No more recipe conflicts! Adds an option to choose the crafting result if more than one is available.|
|[Prism](https://modrinth.com/mod/1OE8wbN0)|A library all about color! Provides lots of color-related functionality for dependent mods.|
|[Prometheus](https://modrinth.com/mod/iYcNKH7W)|A utility mod adding useful commands and player permission handling|
|[Quilted Fabric API (QFAPI) / Quilt Standard Libraries (QSL)](https://modrinth.com/mod/qvIfYCYJ)|The standard libraries of the Quilt ecosystem. Essential for your modding experience on Quilt!|
|[Quilt Kotlin Libraries (QKL)](https://modrinth.com/mod/lwVhp9o5)|Quilt's official Kotlin libraries|
|[Quilt Loading Screen](https://modrinth.com/mod/VPU6VYVP)|A loading screen based off of The Quilt Community's server banner.|
|[Reese's Sodium Options](https://modrinth.com/mod/Bh37bMuy)|Alternative Options Menu for Sodium|
|[Remove Terralith Intro Message](https://modrinth.com/mod/sk4iFZGy)|A datapack/mod to remove the intro message that appears with Terralith.|
|[Resourceful Config](https://modrinth.com/mod/M1953qlQ)|Resourceful Config is a mod that allows for developers to make cross-platform configs|
|[Resourceful Lib](https://modrinth.com/mod/G1hIVOrD)|Resourceful Lib|
|[RightClickHarvest](https://modrinth.com/mod/Cnejf5xM)|Allows you to harvest crops by right clicking|
|[SimpleBackpack_Fabric-1.20.1-1.4.8-1.20.1]()||
|[Simply Swords](https://modrinth.com/mod/bK3Ubu9p)|Adds Spears, Glaives, Chakrams, Katanas, Greathammer/axes, Rapiers, and many more weapons!|
|[3D Skin Layers](https://modrinth.com/mod/zV5r3pPn)|Render the player skin layer in 3d!|
|[Create Slice & Dice](https://modrinth.com/mod/GmjmRQ0A)|Making automation for Farmers Delight more sensible|
|[Sodium Extra](https://modrinth.com/mod/PtjYWJkn)|A Sodium addon that adds features that shouldn't be in Sodium.|
|[Sodium](https://modrinth.com/mod/AANobbMI)|A modern rendering engine for Minecraft which greatly improves performance|
|[Sound Physics Remastered](https://modrinth.com/mod/qyVF9oeo)|A Minecraft mod that provides realistic sound attenuation, reverberation, and absorption through blocks.|
|[Starlight (Fabric)](https://modrinth.com/mod/H8CaAYZC)|Rewrites the light engine to fix lighting performance and lighting errors|
|[Create: Steam 'n' Rails](https://modrinth.com/mod/ZzjhlDgM)|Adding depth to Create's rail network & steam system|
|[Survival Debug Stick](https://modrinth.com/mod/9rVMDWPD)|This mod provides players the ability to craft and use Debug Stick in Survival Mode. You also can easily customize its abilities and restrictions!|
|[TabTPS](https://modrinth.com/mod/cUhi3iB2)|Monitor your server's performance in the tab menu, boss bar, and action bar|
|[Tempad](https://modrinth.com/mod/gKNwt7xu)|Create a portal to anywhere from anywhere|
|[Terralith](https://modrinth.com/mod/8oi3bsk5)|Terralith takes Minecraft's 1.18 massive world generation overhaul, and turns it up to eleven.|
|[Things](https://modrinth.com/mod/VzAGdu9D)|Trinkets and Utilities to enhance casual play|
|[Trinkets](https://modrinth.com/mod/5aaWibi9)|A data-driven accessory mod|
|[VanitySlots](https://modrinth.com/mod/YTVu0oG8)|Vanity armor that covers your normal armor!|
|[Simple Voice Chat](https://modrinth.com/mod/9eGKb6K1)|A working voice chat in Minecraft!|
|[Fabric Waystones](https://modrinth.com/mod/sTZr7NVo)|A better way of transport|
|[YetAnotherConfigLib](https://modrinth.com/mod/1eAoo2KR)|A builder-based configuration library for Minecraft.|
|[Your Options Shall Be Respected (YOSBR)](https://modrinth.com/mod/WwbubTsV)|Your options shall be respected.|
