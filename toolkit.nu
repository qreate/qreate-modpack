#!/usr/bin/env -S nu

# Copyright (C) 2023 (liz / wanesty)
# This file is part of the "qreate-modpack" project.
#
# this project is non-violent software: you can use, redistribute,
# and/or modify it under the terms of the NVPLv7+ as found
# in the LICENSE.md file in the source code root directory or
# at <https://codeberg.org/qreate/qreate-modpack>.
#
# this project comes with ABSOLUTELY NO WARRANTY, to the extent
# permitted by applicable law. For details and future revisions,
# "The Nonviolent Public License Family" official page can be
# found at <https://thufie.lain.haus/NPL.html>.


def main [] {
	print $"Try \'(style green 'toolkit.nu -h'), (style green '--help')\' for more information."
}

# Output a list of included mods, formated in markdown
def "main modlist" [
	modsDir: string = "mods" # Directory containing .pw.toml or .jar files
	--description # Include a description fetched from modrinth's API
] {
	check-folder $modsDir (metadata $modsDir)
	
	if $description == true {
		print $"# modlist\n\n"

		make-list $modsDir --apiDesc
		| update name {|el| $"[($el.name)]\(($el.modpage)\)"}
		| select name description
		| to md
	} else {
		print $"# modlist\n\n"

		make-list $modsDir
		| update name {|el| $"[($el.name)]\(($el.modpage)\)"}
		| select name
		| to md
	}
}

# Output a list of server-side required mods
def "main serverside" [
	modsDir: string = "mods" # Directory containing .pw.toml or .jar files
	--raw # Plain Text output
] {
	check-folder $modsDir (metadata $modsDir)

	if $raw == true {
		make-list $modsDir
		| where side != client
		| get filename
		| to text
	} else {
		make-list $modsDir
		| where side != client
		| select filename name side
	}
}


## Check if folder contains mods, as .pw.toml or .jar
def check-folder [
	checkDir: string # Dir to check 
	checkDirMetadata: record # Metadata for error
] {
	if (ls $checkDir | where name ends-with .pw.toml or name ends-with .jar | is-empty) {
		error make {
			msg: "mods not found"
			label: {
				text: "directory does not exsist or does not contain toml/jar files"
				span: ($checkDirMetadata).span
			}
		}
	}
}

## Generate a list out of modsDir's content 
def make-list [
	modsDir: string
	--apiDesc # Fetch mods description from Modrinth's API
] {
	let jarFiles = (
		if not (ls $modsDir | where name ends-with .jar | is-empty) {
			(ls --short-names $modsDir | where name ends-with .jar).name
			| parse --regex '^(?P<filename>^(?P<name>.*).jar)'
			| insert side null
			| insert update {jar: null}
			| insert download null
		} else {
			null
		}
	)
	
	let withModsPage = (
		if $jarFiles != null { 
			(ls $modsDir | where name ends-with .pw.toml).name | each {|e| open $e}
			| append $jarFiles
		} else {
			(ls $modsDir | where name ends-with .pw.toml).name | each {|e| open $e}
		}
		| insert modpage {|ta|
			match ($ta.update | columns).0 {
				"modrinth" => $"https://modrinth.com/mod/($ta.update.modrinth.mod-id)"
				"curseforge" => (
					$"https://www.curseforge.com/minecraft/search?search=($ta.name)"
					| str replace --all ' ' '%20'
				)
				"jar" => null
			}
		}
		| sort-by --ignore-case filename
	)

	if $apiDesc == true {
		let modrinthDesc = (
			http get https://api.modrinth.com/v2/projects?ids=(
				$withModsPage
				| where ($it.update | columns).0 == "modrinth"
				| each {|it| $it.update.modrinth.mod-id}
				| to json --raw
			)
		)
	
		$withModsPage
		| insert description {|ta|
			if ($ta.update | columns).0 == "modrinth" {
				(
					$modrinthDesc
					| where ($it.id) == $ta.update.modrinth.mod-id
				).description.0
			} else {
				null
			}
		}
	} else {
		$withModsPage
	}
}


# Styling
def style [
	color:string # Color code
	text:string # Text to style
] {
	$"(ansi ($color))($text)(ansi reset)"
}