# Versioning


**Major Releases**: Frequent major releases (e.g., 001, 002, 003) that are likely to break compatibility with prior major versions.  
They deliver feature updates and significant bug fixes.

**Minor Releases**: Sequentially follow major releases with letters (e.g., 001a, 001b).  
These releases focus on maintaining client/server compatibility within the same major version and offer client-side updates and minor bug fixes.

**Recommendations**:
- Ensure client and server use the same version for stability.
- Stay up-to-date if you want the latest features and fixes.
- Server owners using this project should consider adding the modpack version to their server's MOTD.