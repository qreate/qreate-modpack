<img align="right" width="20%" src="https://codeberg.org/qreate/qreate-art/raw/branch/main/icons/icon-cog256.png">

# qreate
a 1.20.1 minecraft modpack using the [Quilt mod-loader](https://quiltmc.org/en/) and centered on [Create fabric](https://modrinth.com/mod/create-fabric)<br>
<sub>this repository holds the files for building a [.mrpack](https://docs.modrinth.com/docs/modpacks/format_definition/) modpack using [packwiz](https://github.com/packwiz/packwiz)</sub>

<br>

see the [list of mods](modlist.md)

**changelogs** and **modpack download** are available on the [releases page](https://codeberg.org/wanesty/qreate/releases)