<h1 align="center">
	Copying terms
</h1>

This work is currently hosted in a Codeberg repository at the following URL :  
https://codeberg.org/wanesty/qreate

The files contained within this repository inherit the terms and conditions of the [Nonviolent Public License](LICENSE.md), unless otherwise specified below. <sup>[NPL Official Page](https://thufie.lain.haus/NPL.html)</sup>

---